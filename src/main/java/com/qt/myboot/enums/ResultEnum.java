package com.qt.myboot.enums;

import com.qt.myboot.interfaces.ResultStatus;

public enum ResultEnum  implements ResultStatus{

    USER_NOT_EXIST(404, "该账号不存在!"),
    USER_SUSPEND(10001, "该账号已被冻结!"),
    WRONG_PASSWORD(10002, "您输入的密码不正确!"),
    ACCOUNT_LOCK(10004, "密码连续输入错误超过5次，锁定半小时!"),
    REGISTER_CODE_ERROR(10005, "验证码错误!"),
    ENTERED_PASSWORDS_DIFFER(10006, "两次输入的密码不一致"),
    PASSWORD_AUTHENTICATION_ERROR(10007, "密码长度8~16位，其中数字，字母和符号至少包含两种!"),
    ACCOUNT_ERROR(10008, "该用户名已被使用!"),
    LOGIN_SUCEESS(200,"登录成功！"),
    LOGIN_FAILED(500,"用户名或者密码不正确！"),
    UNDEFINED_ERROR(10009,"未知错误,请联系网站管理员！"),
    MANAGE_SUCCESS(200,"处理成功!"),
    MANAGE_FAILED(500,"处理失败!"),
    BOOT_EXCEPTION(500, "发生异常!"),
    USER_ADD_DUPLICATION(500, "已存在"),
    USER_TOKEN_EXPIRED(500, "token不正确"),
    USER_NO_PERMISSION(500, "无权限访问"),
    SQL_OPTIMISTIC_ERROR(500, "数据库出现锁异常"),

    ;

    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
