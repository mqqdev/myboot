package com.qt.myboot.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.qt.myboot.dao.ProductRepository;
import com.qt.myboot.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Despription:
 *
 * @Autor: xusl
 * @Date: 2019/6/13 17:56
 * @Version: 1.0
 **/
@Component(value = "productService")
@Service(version = "1.0.0", interfaceClass = ProductService.class, timeout = 3000, retries = 1)
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    public void add(Product product) throws Exception{
        productRepository.save(product);

    }

    public void update(Product product) throws Exception{
        productRepository.updateProductById(product.getProductName(), product.getId());

    }

    public Product findOne(Integer id) throws Exception{
        return productRepository.findById(id).get();
    }
}
