package com.qt.myboot;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.qt.myboot.utils.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
@EnableDubbo
public class MybootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybootApplication.class, args);
	}

	@Bean
	public IdWorker idWorker() {
		return new IdWorker();
	}

}
