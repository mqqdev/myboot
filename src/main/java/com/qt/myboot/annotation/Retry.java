package com.qt.myboot.annotation;

import java.lang.annotation.*;

/**
 * Despription: 重试机制注解
 *
 * @Autor: xusl
 * @Date: 2019/6/13 19:22
 * @Version: 1.0
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Retry {

    /**重试次数 默认重试1次**/
    int retries() default 1;
}
