package com.qt.myboot.dao;

import com.qt.myboot.entity.UserRolePermissionRelation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RolePermissionsRepository extends BaseRepository<UserRolePermissionRelation, Integer>{

    @Query(value = "SELECT\n" +
            "\tb.permissions_name\n" +
            "FROM\n" +
            "\tboot_role_permissions_relation a\n" +
            "INNER JOIN boot_permissions b ON a.permissions_id = b.id\n" +
            "WHERE\n" +
            "\ta.role_id IN ?1", nativeQuery = true)
    List<String> findPermissionsNameByRoleIdIn(List<Integer> roleId);

}
