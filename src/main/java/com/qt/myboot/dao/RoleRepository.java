package com.qt.myboot.dao;

import com.qt.myboot.entity.UserRole;

public interface RoleRepository extends BaseRepository<UserRole, Integer>{

}
