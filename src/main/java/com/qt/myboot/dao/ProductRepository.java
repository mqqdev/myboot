package com.qt.myboot.dao;

import com.qt.myboot.entity.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends BaseRepository<Product, Integer>{

    @Query(value = "update boot_product set product_name = ?1 where id = ?2", nativeQuery = true)
    @Modifying
    public int updateProductById(String productName, Integer id);

}
