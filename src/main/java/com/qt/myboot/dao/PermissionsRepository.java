package com.qt.myboot.dao;

import com.qt.myboot.entity.UserPermissions;

public interface PermissionsRepository extends BaseRepository<UserPermissions, Integer>{

}
