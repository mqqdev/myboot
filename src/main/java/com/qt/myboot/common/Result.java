package com.qt.myboot.common;

import com.qt.myboot.enums.ResultEnum;

public class Result<T> extends BaseResult{

    public  Result(Integer code, String msg, boolean success) {
        super(code, msg, success);
    }

    public Result(ResultEnum resultEnum, boolean success) {
        super(resultEnum.getCode(), resultEnum.getMsg(), success);

    }

    public Result(ResultEnum resultEnum, boolean success, Object data) {
        super(resultEnum.getCode(), resultEnum.getMsg(), success, data);

    }

    public static <T> Result<T> success() {
        return new Result<T>(ResultEnum.MANAGE_SUCCESS.getCode(), ResultEnum.MANAGE_SUCCESS.getMsg(), true);
    }

    public static <T> Result<T> success(Object data) {
        return new Result<T>(ResultEnum.MANAGE_SUCCESS, true, data);
    }


    public static <T> Result<T> failed() {
       return new Result<T>(ResultEnum.MANAGE_FAILED.getCode(), ResultEnum.MANAGE_FAILED.getMsg(), false);
    }

    public static <T> Result<T> failed(Object data) {
        return new Result<T>(ResultEnum.MANAGE_FAILED, false, data);
    }




}
