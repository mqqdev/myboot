package com.qt.myboot.exception;


import com.qt.myboot.enums.UserErrorCodeEnum;
import com.qt.myboot.interfaces.ErrorCode;


public class UserErrorCodeException extends RuntimeException {

    private static final long serialVersionUID = -2396422934408894887L;

    private ErrorCode errorCode;

    public UserErrorCodeException(String code, String desc) {
        super(desc);
        this.errorCode.setCode(code);
        this.errorCode.setDesc(desc);
    }

    public UserErrorCodeException() {

    }

    /**
     * 带错误码的构造函数
     *
     * @param errorCode
     */
    public UserErrorCodeException(ErrorCode errorCode) {
        super(errorCode.getDesc());
        this.errorCode = errorCode;
    }

    /**
     * 获取错误码
     *
     * @return
     */
    public ErrorCode getErrorEnum() {
        return errorCode;
    }

}
