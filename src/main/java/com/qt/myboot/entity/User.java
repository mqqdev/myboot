package com.qt.myboot.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@Entity
@ApiModel(value = "用户")
@Table(name = "boot_user")
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable{

    private static final long serialVersionUID = -5085644867462686336L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "id")
    private Integer id;

    @NotBlank(message = "用户姓名不能为空")
    @Size(min = 3, max = 10, message = "姓名长度不正确")
    @ApiModelProperty(value = "用户姓名", example = "zhangsan")
    private String userName;

    @NotBlank
//    @Size(min = 6, max = 10, message = "密码长度不正确")
    @ApiModelProperty(value = "用户密码", example = "123456")
    private String password;

    @NotBlank(message = "邮箱不能为空")
    @Email(regexp = "^[a-z0-9]+([._\\\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$", message = "邮箱不正确")
    @ApiModelProperty(value = "邮箱", example = "33@163.com")
    private String email;

    @NotBlank(message = "手机不能为空")
    @Pattern(regexp = "^1([38]\\d|5[0-35-9]|7[3678])\\d{8}$", message = "手机号不正确")
    @ApiModelProperty(value = "手机", example = "13567678989")
    private String phone;

    @CreatedDate
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @LastModifiedDate
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
