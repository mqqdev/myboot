//package com.qt.myboot.filter;
//
//import com.qt.myboot.enums.UserErrorCodeEnum;
//import com.qt.myboot.exception.UserErrorCodeException;
//import com.qt.myboot.utils.JwtUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * 请求鉴权拦截器
// */
//@Slf4j
//@Component
//public class TokenValidateInterceptor extends HandlerInterceptorAdapter {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        boolean flag = true;
//
//        // 获取 HTTP HEAD 中的 TOKEN
//        String authorization = request.getHeader("Authorization");
//        // 校验 TOKEN
//        flag = StringUtils.isNotBlank(authorization) ? JwtUtil.checkJWT(authorization) : false;
//        // 如果校验未通过，返
//        // 回 401 状态
//        if (!flag) {
//            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//            throw new UserErrorCodeException(UserErrorCodeEnum.US35);
////            return flag;
//        }
//
//        return flag;
//    }
//}