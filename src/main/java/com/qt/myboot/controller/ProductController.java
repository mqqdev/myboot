package com.qt.myboot.controller;

import com.qt.myboot.annotation.Retry;
import com.qt.myboot.common.Result;
import com.qt.myboot.entity.Product;
import com.qt.myboot.service.ProductService;
import com.qt.myboot.service.ProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Despription:
 *
 * @Autor: xusl
 * @Date: 2019/6/13 17:59
 * @Version: 1.0
 **/
@RestController
@RequestMapping(value = "product")
@Slf4j
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/add")
    public Result add(Product product) throws Exception{
        productService.add(product);
        return Result.success();
    }

    /**
     * @Description: 测试 多个线程对同一条数据修改 由于version不同  出现乐观锁现象
     * @Author: xusl
     * @Date: 2019/6/13 20:48
     * @Param: [product]
     * @return: com.qt.myboot.common.Result
     **/
    @PostMapping("/update")
    @Retry  // 异常则重试1次
    public Result update(Product product) throws Exception{
//        productService.update(product);
        Product vo = productService.findOne(product.getId());
        log.info(vo.toString());
        new Thread(() -> {
            vo.setProductName("Macbook Pro 2019款i7款标压1TB 15.4英寸");
            try {
                productService.add(vo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        vo.setProductName("Matebook X 2019款i7款标压1TB 15.4英寸");
        productService.add(vo);
        return Result.success();
    }
}
